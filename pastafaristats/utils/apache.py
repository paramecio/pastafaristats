import requests

def stat(obj_stats):
    
    #new_obj_stats['apache_data']={'status': 1}
    
    url='http://127.0.0.1/server-status?auto'
    
    try:
    
        r=requests.get(url)

        data=r.text.split("\n")
        
        final_data={v.split(':')[0].strip():v.split(':')[1].strip() for v in data if v.find(':')!=-1}
        
        final_data['status']=1
        
        obj_stats['apache_data']=final_data

    except:
        obj_stats['apache_data']['status']=0

    return obj_stats

if __name__=='__main__':
    
    print(stat({}))
    

