from PyQt5.QtGui import * 
from PyQt5.QtWidgets import * 
from PyQt5.QtCore import *
from pastafaristats import send_info_daemon
from time import sleep
import os
import psutil
from pathlib import Path
import signal

#Get user_home and environment variables

user_home=str(Path.home())

path_config=user_home+'/.config/pastafari/stats.cfg'

# Create configuration

Path(os.path.dirname(path_config)).mkdir(parents=True, exist_ok=True)

if not os.path.isfile(path_config):
    config_data="""[DEFAULT]
    url_server={}
    """.format('http://localhost:5000/api_token')
    
    with open(path_config, 'w') as f:
        f.write(config_data)

# Get functions

def quit_app():
    print('Quitting Application...')
    #print(QThreadPool.globalInstance().activeThreadCount())
    #QThreadPool.globalInstance().exit()
    """
    thread=QThread.currentThread()
    print(thread)
    thread.quit()
    """
    #QThreadPool.globalInstance().tryTake(p)
    app.quit()
    
    # Get pid
    
    pid=os.getpid()
    
    p=psutil.Process(pid)
    
    p.terminate()
    
    exit(0)
    
    # Killing using pid
    
    #p.stop()
def show_config_win():
    
    winconfig.show()

def send_data():
    print('Init daemon...')
    
    url_monit, group=send_info_daemon.load_config()
    
    while True:
        
        send_info_daemon.run(url_monit)
        
        sleep(120)
    

class ProcessRunnable(QRunnable):
    
    def __init__(self, target, args):
        QRunnable.__init__(self)
        #self.setAutoDelete(True)
        #self.waitForDone(100)
        self.t = target
        self.args = args

    def run(self):
        self.t(*self.args)

    def start(self):
        #QThreadPool.globalInstance().waitForDone(100)
        QThreadPool.globalInstance().start(self)
    """
    def stop(self):
        print('Stopping daemon...')
        del self.t
    """
    
class WinConfig(QWidget):
    def __init__(self, url_monit='http://192.168.1.51/monit'):
        super().__init__()
        
        url_monit, group=send_info_daemon.load_config()
        
        self.text=QLabel("Change monitoritation url, example: http://192.168.1.51/monit/api_token")
        self.input=QLineEdit(url_monit)
        self.button=QPushButton("Save")
        
        self.layout=QVBoxLayout(self)
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.input)
        self.layout.addWidget(self.button)
  
        self.button.clicked.connect(self.save_config)
        
    
    def save_config(self):
        
        config_data="""[DEFAULT]
        url_server={}
        """.format(self.input.text())
        
        with open(path_config, 'w') as f:
            f.write(config_data)
        
        print('Config saved...')
                
        self.hide()
  
app = QApplication([])
app.setQuitOnLastWindowClosed(False)
  
# Adding an icon

icon_png=os.path.realpath(os.path.dirname(__file__))+'/icon.png'

icon = QIcon(icon_png)
  
# Adding item on the menu bar
tray = QSystemTrayIcon()
tray.setIcon(icon)
tray.setVisible(True)
  
# Creating the options

menu = QMenu()

option1 = QAction("Pastafari monitoring")
menu.addAction(option1)

option2 = QAction("Setup Monitoring url")
option2.triggered.connect(show_config_win)
menu.addAction(option2)
  
# To quit the app
quit = QAction("Quit")
quit.triggered.connect(quit_app)
menu.addAction(quit)
  
# Adding options to the System Tray
tray.setContextMenu(menu)

# Prepare url monitoritation

winconfig=WinConfig()

winconfig.resize(400, 200)

# Begin background process

p = ProcessRunnable(target=send_data, args=())
p.start()

# Catch sigint

def catch_signal(sig, frame):
    print('Exiting from app...')
    pid=os.getpid()
    
    p=psutil.Process(pid)
    
    p.terminate()
    exit(0)

signal.signal(signal.SIGINT, catch_signal)

# Begin tray loop

app.exec_()
