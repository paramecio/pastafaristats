from pastafaristats import send_info_daemon

url, group=send_info_daemon.load_config()

if __name__=='__main__':
    send_info_daemon.run(url, group)

