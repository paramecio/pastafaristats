#!/usr/bin/python3 -u

import psutil
import json
import urllib.request
import urllib.parse
import ssl
from time import sleep
import configparser
import os
from sys import exit
from pathlib import Path
import signal
from socket import getfqdn
import argparse
#from paramecio.citoplasma import datetime
import datetime

import sched, time
from importlib import import_module


#url="http://url/to/info"

user_home=str(Path.home())

hostname=getfqdn()

modules_imported={}

def load_config():
    
    yes_config=False
    
    config = configparser.ConfigParser(interpolation=None)

    if os.path.isfile(user_home+'/.config/pastafari/stats.cfg'):
        
        config.read(user_home+'/.config/pastafari/stats.cfg')
        
        yes_config=True

    elif os.path.isfile('/etc/pastafari/stats.cfg'):
        
        config.read('/etc/pastafari/stats.cfg')
        
        yes_config=True
    
    else:
        #Check command line
        parser = argparse.ArgumentParser(description='Script for send data to monitor server')
        parser.add_argument('--config', help='Path from configuration file', required=True)
        args = parser.parse_args()
        
        file_config=args.config
        
        config.read(file_config)
        
        yes_config=True
    
    if not yes_config:
        
        exit("Sorry, cannot load config file")

    if not 'DEFAULT' in config:
        
        exit("Sorry, config file need [DEFAULT] section")

    if not 'url_server' in config['DEFAULT']:
        
        exit("Sorry, config file need url_server variable in [DEFAULT] section")
        
    url=config['DEFAULT']['url_server']
    
    group=''
    
    if 'group' in config['DEFAULT']:
        group=config['DEFAULT']['group']
    
    
    modules={}
    
    if 'modules' in config['DEFAULT']:
        arr_modules=config['DEFAULT']['modules'].split(',')
        
        #load modules
    
        for module in arr_modules:
            
            if not module in modules_imported:
    
                modules_imported[module]=import_module(module)
        
    
    return url, group


def run(url, group=''):
    
    network_info=psutil.net_io_counters(pernic=False)

    network_devices=psutil.net_if_addrs()

    cpu_idle=psutil.cpu_percent(interval=1)
    
    cpus_idle=psutil.cpu_percent(interval=1, percpu=True)

    cpu_number=psutil.cpu_count()

    disk_info=psutil.disk_partitions()

    partitions={}

    for disk in disk_info:
        
        partition=str(disk[1])
        partitions[partition]=psutil.disk_usage(partition)
        #print(partition+"="+str(partitions[partition]))

    dev_info={}

    mem_info=psutil.virtual_memory()

    obj_stats={'net_info': network_info, 'cpu_idle': cpu_idle, 'cpus_idle': cpus_idle, 'cpu_number': cpu_number, 'disks_info': partitions, 'mem_info': mem_info, 'hostname': hostname, 'group': group}

    for module in modules_imported.values():
        obj_stats=module.stat(obj_stats)

    json_info=json.dumps(obj_stats)

    data = urllib.parse.urlencode({'data_json': json_info})

    data = data.encode('ascii')

    try:

        if url[:5]=='https':
            
            # For nexts versions 3.5
            
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
            
            content=urllib.request.urlopen(url, data, context=ctx)
            
        else:

            content=urllib.request.urlopen(url, data)

    except Exception as e:
        
        print('Cannot connect to data server -> '+str(e))
        
        #Reload config
        url, group=load_config()
        

def run_start(sc):

    # Get config from /etc/pastafari or ~/.config/pastafari

    # Load configuration
    
    url, group=load_config()
    
    sec=str(datetime.datetime.utcnow())
    
    print('Send monitoring in %s...' % sec)
    
    num_seconds=int(sec[17:19])
    
    total_seconds=60
    
    final_seconds=total_seconds-num_seconds
    
    sc.enter(final_seconds, 1, run_start, (sc,))
    
    run(url, group)
    
    """
    while True:
        
        run(url, group)
        
        sleep(60)
    """
    
def start():
    
    #Wait seconds to 
    
    print('Syncing time collection...')
    
    while True:
        
        sleep(2)
    
        #sec=datetime.now()
        sec=str(datetime.datetime.utcnow())
        
        if sec[17:18]=='0':
            break
    
    print('Begin scheduler monit in %s...' % sec)
    
    s=sched.scheduler(time.time, time.sleep)
    
    run_start(s)
    
    s.run()

if __name__=='__main__':
    
    def catch_signal(sig, frame):
        print('Exiting...')
        exit(0)

    signal.signal(signal.SIGINT, catch_signal)

    start()
    
    
