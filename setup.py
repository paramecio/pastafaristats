#!/usr/bin/env python3

import sys
import os
from setuptools import setup, find_packages


if sys.version_info < (3, 6):
    raise NotImplementedError("Sorry, you need at least Python 3.6 for use pastafaristats.")

setup(name='pastafaristats',
      version='1.0.2',
      description='Simple scripts for send basic data of a server how complement to other stats solutions more complex.',
      author='Antonio de la Rosa Caballero',
      author_email='antonio.delarosa@coesinfo.com',
      url='https://bitbucket.org/paramecio/pastafaristats/',
      packages=['pastafaristats', 'pastafaristats.utils'],
      include_package_data=True,
      install_requires=['psutil'],
      entry_points={'console_scripts': [
        'pastafaristats = pastafaristats.send_info_daemon:start',
      ]},
      zip_safe=False,
      license='GPLV3',
      platforms = 'any',
      classifiers=['Development Status :: 1 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLV3 License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10'
        ],
     )
