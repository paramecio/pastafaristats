# PastafariStats, a simple collector for basic stats. 

## Installation

You can install PastafariStats with its command.

`pip3 install git+https://bitbucket.org/paramecio/pastafaristats`

### Add systemd service

If your linux distro use systemd, you can initialize the script with this systemd unit

```
# Save it in /etc/systemd/system/pastafaristats.service

[Unit]
Description=Pastafari Stats
After=syslog.target
After=network.target

[Service]
Type=simple
User=pzoo
Group=pzoo
ExecStart=/path/to/pastafaristats
Restart=always
Environment=PYTHONUNBUFFERED=1

[Install]
WantedBy=multi-user.target
```

### Configuration

You can configure the script placing a config file in /etc/pastafari/stats.cfg

```
[DEFAULT]

url_server=http://url_server_collect_data/
group=server-group
```

Where url_server_collect_data is the hostname of server collecting data, api_key_selected, the api key of server for accept the data, and ip_server, the ip of the server that send the data. 

Group: A server can belong to group. If you have a servers in a network, you can group them with the same group name. 

# Configuration for Windows 10, Windows Server 2012, etc. 

Windows sucks, but you can need monitoring windows servers or desktops.

First, install python 3 (last stable version if you can, actually python 3.9). You can download from [here](https://www.python.org/downloads/)

### Important

**You must install python with "Add python.exe to windows path" enabled.**

You need install git. You should install it in Windows path too.

You can install PastafariStats with its command how in Linux/*nix.

`git clone https://bitbucket.org/paramecio/pastafaristats`

If you are in C:\Users\my_user directory, pastafaristats will be download in C:\Users\my_user\pastafaristats

Next, you need create the config file. For example, if you have an user called "my_user", you can install configuration file in C:/Users/my_user/config/stats.cfg with the same contents how in Linux.

```
[DEFAULT]

url_server=http://url_server_collect_data/
group=server-group
```

Now, you need install the python script how service. You can use a classic manager services for windows called [NSSM](https://nssm.cc/). If you use Windows 10 64 bits, you can download from [here](https://nssm.cc/ci/nssm-2.24-101-g897c7ad.zip)

Now, you can execute this command:

nssm install MonitService

A GUI will be showed with many options. Basic options in Application block are:

- Path: C:\Users\my_user\AppData\Local\Programs\Python\Python39\python.exe

Here you should set the python executable path. 

- Startup: C:\Users\my_user\pastafaristats\pastafaristats\

Here you should set the pastafaristats real directory.

- Arguments: C:\Users\developer\pastafaristats\pastafaristats\send_info_daemon.py --config=C:\Users\my_user\config\stats.cfg

Here you shoud set the argumentos. send_info_daemon.py is the daemon that send the data to the server, --config is an option for define where config file stay.

There other options how I/O where you can define a file log for check errors if you have problems with the service.

All's done!, you can init your service with **nssm start MonitService** and let's go.

# Developing plugins.

You can add simple python scripts how plugins for add new collections to your data.

The python scripts need have the next structure:

```python
def stat(obj_stats):

    obj_stats['test_data']={'pos': 0}

    return obj_stats
```

You define a function with name *stat* and arguments *obj_stats*

*obj_stats* is a python dictionary. You can add new data or manipulate original collected data for your own proposites. The function returns *obj_stats* modified.

Next, you need modify your configuration with this line:

```

```

```
[DEFAULT]

url_server=http://url_server_collect_data/
group=server-group
modules=path.to.script, path.to.script2
```

"path.to.script" is the python script path for import it into pastafaristats. You can use many scripts, but if you need many stats, is recommendable use stats collectors how collectd, netdata, etc.


